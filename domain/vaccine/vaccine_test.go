package vaccine

import (
	"testing"
	"time"

	"github.com/google/uuid"
)

func TestVaccine_HasExpired(t *testing.T) {
	type fields = Vaccine
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Valid Vaccine",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			want: false,
		},
		{
			name: "Expired Vaccine",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, -5),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.HasExpired(); got != tt.want {
				t.Errorf("Vaccine.HasExpired() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_IsApplicable(t *testing.T) {
	type fields = Vaccine
	type args struct {
		species string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Species match",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{SpeciesCanine},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args: args{species: SpeciesCanine},
			want: true,
		},
		{
			name: "Species doesnt match",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{SpeciesCanine},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args: args{species: SpeciesFeline},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.IsApplicable(tt.args.species); got != tt.want {
				t.Errorf("Vaccine.IsApplicable() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_IsAvailable(t *testing.T) {
	type fields = Vaccine
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Vaccine Available",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now(),
			},
			want: true,
		},
		{
			name: "Vaccine Reserved",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusReserved,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
		{
			name: "Vaccine Applied",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusApplied,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.IsAvailable(); got != tt.want {
				t.Errorf("Vaccine.IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_IsReserved(t *testing.T) {
	type fields = Vaccine
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Vaccine Available",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
		{
			name: "Vaccine Reserved",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusReserved,
				ExpirationDate: time.Now(),
			},
			want: true,
		},
		{
			name: "Vaccine Applied",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusApplied,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.IsReserved(); got != tt.want {
				t.Errorf("Vaccine.IsReserved() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_IsApplied(t *testing.T) {
	type fields = Vaccine
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Vaccine Available",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
		{
			name: "Vaccine Reserved",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusReserved,
				ExpirationDate: time.Now(),
			},
			want: false,
		},
		{
			name: "Vaccine Applied",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{},
				Company:        "",
				Batch:          "",
				Status:         StatusApplied,
				ExpirationDate: time.Now(),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.IsApplied(); got != tt.want {
				t.Errorf("Vaccine.IsApplied() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_IsEffective(t *testing.T) {
	type fields = Vaccine
	type args struct {
		agent string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Is effective against some agent",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{"Cinomose canina", "Parvovírus canino"},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args: args{agent: "Cinomose canina"},
			want: true,
		},
		{
			name: "Is effective against some agent",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{},
				Effectiveness:  []string{"Cinomose canina", "Parvovírus canino"},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args: args{agent: "Parainfluenza vírus"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if got := v.IsEffective(tt.args.agent); got != tt.want {
				t.Errorf("Vaccine.IsEffective() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVaccine_Apply(t *testing.T) {
	type fields = Vaccine
	type args struct {
		species string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Valid vaccine",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{SpeciesCanine},
				Effectiveness:  []string{"Cinomose canina"},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args:    args{species: SpeciesCanine},
			wantErr: false,
		},
		{
			name: "Expired vaccine",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{SpeciesCanine},
				Effectiveness:  []string{"Cinomose canina"},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, -5),
			},
			args:    args{species: SpeciesCanine},
			wantErr: true,
		},
		{
			name: "Species mismatch",
			fields: fields{
				ID:             uuid.New(),
				CommercialName: "",
				ScientificName: "",
				Species:        []string{SpeciesCanine},
				Effectiveness:  []string{"Cinomose canina"},
				Company:        "",
				Batch:          "",
				Status:         StatusAvailable,
				ExpirationDate: time.Now().AddDate(0, 0, 5),
			},
			args:    args{species: SpeciesFeline},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &Vaccine{
				ID:             tt.fields.ID,
				CommercialName: tt.fields.CommercialName,
				ScientificName: tt.fields.ScientificName,
				Species:        tt.fields.Species,
				Effectiveness:  tt.fields.Effectiveness,
				Company:        tt.fields.Company,
				Batch:          tt.fields.Batch,
				Status:         tt.fields.Status,
				ExpirationDate: tt.fields.ExpirationDate,
			}
			if err := v.Apply(tt.args.species); (err != nil) != tt.wantErr {
				t.Errorf("Vaccine.Apply() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
