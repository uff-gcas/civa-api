package vaccine

import (
	"errors"
	"time"

	"github.com/google/uuid"
)

const (
	StatusAvailable = "AVAILABLE"
	StatusApplied   = "APPLIED"
	StatusReserved  = "RESERVED"
	SpeciesCanine   = "CANINE"
	SpeciesFeline   = "FELINE"
)

type Vaccine struct {
	ID             uuid.UUID `json:"id,omitempty"`
	CommercialName string    `json:"commercial_name,omitempty"`
	ScientificName string    `json:"scientific_name,omitempty"`
	Species        []string  `json:"species,omitempty"`
	Effectiveness  []string  `json:"effectiveness,omitempty"`
	Company        string    `json:"company,omitempty"`
	Batch          string    `json:"batch,omitempty"`
	Status         string    `json:"status,omitempty"`
	ExpirationDate time.Time `json:"expiration_date,omitempty"`
}

func (v *Vaccine) Apply(species string) error {
	err := v.mustBeValid(species)
	if err != nil {
		return err
	}
	v.Status = StatusApplied
	return nil
}

func (v *Vaccine) mustBeValid(species string) error {
	if v.HasExpired() {
		return errors.New("Vaccine has expired")
	}
	if !v.IsApplicable(species) {
		return errors.New("Vaccine species doesnt match")
	}
	return nil
}

func (v *Vaccine) HasExpired() bool {
	today := time.Now()
	return v.ExpirationDate.Before(today)
}

func (v *Vaccine) IsApplicable(species string) bool {
	for _, s := range v.Species {
		if s == species {
			return true
		}
	}
	return false
}

func (v *Vaccine) IsAvailable() bool {
	return v.Status == StatusAvailable
}

func (v *Vaccine) IsReserved() bool {
	return v.Status == StatusReserved
}

func (v *Vaccine) IsApplied() bool {
	return v.Status == StatusApplied
}

func (v *Vaccine) IsEffective(agent string) bool {
	for _, a := range v.Effectiveness {
		if a == agent {
			return true
		}
	}
	return false
}
